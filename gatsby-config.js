module.exports = {
    siteMetadata: {
        title: `Palakkad Libre software Users Society | Formerly Palakkad GNU/Linux User Society`,
        description: `Palakkad Libre software Users Society [Formerly Palakkad GNU/Linux User Society] supports FOSS related activities in Palakkad.PLUS is dedicated to promoting computer users' rights to use, study, copy, modify, and redistribute computer programs. PLUS	promotes the development and use of free software, particularly the GNU operating system, used widely in its GNU/Linux variant.`,
        author: 'admin@plus',
    },
    plugins: [
        `gatsby-plugin-react-helmet`,
        `gatsby-transformer-sharp`,
        `gatsby-plugin-sass`,
        `gatsby-plugin-sharp`,
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `Palakkad Libre software Users Society`,
                short_name: `PLUS`,
                start_url: `/`,
                background_color: `#16a085`,
                theme_color: `#16a085`,
                display: `standalone`,
                icon: `src/images/plus-logo.png`, // This path is relative to the root of the site.
            },
        },
        'gatsby-plugin-catch-links',
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                path: `${__dirname}/src/pages`,
                name: 'pages',
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                path: `${__dirname}/src/content`,
                name: `markdown-pages`,
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `images`,
                path: `${__dirname}/src/images`,
            },
        },
        {
            resolve: 'gatsby-transformer-remark',
            options: {
                plugins: [
                    'gatsby-remark-relative-images',
                    {
                        resolve: 'gatsby-remark-images',
                        optional: {
                            maxWidth: 750,
                            linkImagesToOriginal: false,
                            tracedSVG: true,
                        },
                    },
                ],
            },
        },
        // this (optional) plugin enables Progressive Web App + Offline functionality
        // To learn more, visit: https://gatsby.dev/offline
        //  `gatsby-plugin-offline`, Disable during initial devloping
    ],
};
